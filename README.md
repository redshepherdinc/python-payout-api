![logo](https://redshepherdmedia.s3-us-west-2.amazonaws.com/red_logo_64.png "logo")

# Red Shepherd Python Payout API Docs

### To Make Payouts Work, you need to do the following steps

### Step 1. Create a Payout Account, ie the 3rd party you need to pay the funds to.

### Step 2. Create an ACH token using _redpay_ API for their ACH bank info (routing number and account number)

### Step 3. Add this token to their account

### Step 4. Add a Payout Rate to their account which determines the parameters and frequency of their payouts

### Step 5. Do not forget to Start Payouts on that account, otherwise the scheduler will not pick this account for payout scheduling

## All these API Calls and steps are included in this documentation, if you have any questions please email support@redshepherd.com

---

---

## _Follow these steps for Simple API Integration_

---

### Step 1 a- Adding the payout library using pip

> **_Add the payout library to your code using pip_**

```python
> pip install payout
```

---

### Step 1 b - Create a Payout object for secure API methods

> **_Create a payout API object which lets you access all the payout functions_**

```python
from payout import Payout

### REPLACE app and key with your PROD keys and use a valid account
### These are DEMO Keys which you can safely use for testing
app = "DEMO";
key = "MIIBojANBgkqhkiG9w0BAQEFAAOCAY8AMIIBigKCAYEAtsQxNp3vmKVNYIxfWSi0LIRgCnPaMn0MUNouxgrs4zmg4cnvSeQ3I8YP03YbpXuWA80RvOw/nWErYAKomniJw8Y+xexMfBQ5sgJgewn3ZnRPNM9Y4Z62gwfIlsrs7Bwvpz9uUtLgeQLl1ffNaumnu1IBrqRps0EZ1QyDuu41UckTyo31C40Wez6IbeMfZeusrmPlIWqyBacdviJ5zHCA3zHNq86QMnB8HOP1U81HOSs6GTTelhD7lCoJ+fHKHxcz0MDr37fNpKpC57B0/20wBXFp9tlVtSkHcIty1lyNk2/HDH8knCdqkZk+fCvWgGwdex41x8/rM+LKC13c5J/yG6Gb2PnKhwNk4lvvnz73YAdqTUJ7qNrdtWVnOTWfbMBiNlpBCVqt8xY8UK6u83AVWrWXse0xe2Pn/kRqlXmxWT0mGEoCavjvZ9lQUL7LXAXZ1dff9r+oFUZo6xDQ3ER/OTIKa4jpvaI9S/J1drsrI1f9kkMWFwEh48dCPYplGSxzAgMBAAE=";

### Use this payout object for all the API calls
payout = Payout(app, key)

```

---

---

## _Payout Account API Functions_

---

### Create a Payout Account

> **_Create a new Payout Account in the system which will later be configure to receive payouts from the Red Shepherd payout scheduler_**
>
> > id should be unique identifier for each Payout Account
> > **_rest of the request fields are self explanatory_**

```python
import json

request = {
  "id": "PZZA",
  "name": "Pizza Pizza",
  "website": "www.pizzapizza.com",
  "address1": "123 Main Street",
  "address2": "Suite 2001",
  "city": "Chicago",
  "state": "IL",
  "zip": "60004",
  "country": "US",
  "contactName": "John Smith",
  "contactPhone": "312-899-8999",
  "contactEmail": "john@pizzapizza.com",
  "status": "A"
}

print("CREATE PAYOUT ACCOUNT request >>>")
print(json.dumps(request, indent=2))

response = payout.CreatePayoutAccount(request)

print("CREATE PAYOUT ACCOUNT response >>>")
print(json.dumps(response, indent=2))

```

**_CreatePayoutAccount Request Example_**

```python
{
  "id": "PZZA",
  "name": "Pizza Pizza",
  "website": "www.pizzapizza.com",
  "address1": "123 Main Street",
  "address2": "Suite 2001",
  "city": "Chicago",
  "state": "IL",
  "zip": "60004",
  "country": "US",
  "contactName": "John Smith",
  "contactPhone": "312-899-8999",
  "contactEmail": "john@pizzapizza.com",
  "status": "A"
}
```

**_CreatePayoutAccount Response_**

```python
{
  "id": "PZZA",
  "app": "DEMO",
  "logoUrl": null,
  "name": "Pizza Pizza",
  "address1": "123 Main Street",
  "address2": "Suite 2001",
  "city": "Chicago",
  "state": "IL",
  "zip": "60004",
  "country": "US",
  "contactName": "John Smith",
  "contactPhone": "312-899-8999",
  "contactEmail": "john@pizzapizza.com",
  "website": "www.pizzapizza.com",
  "status": "A"
}
```

---

### Update a Payout Account

> **Update an existing Payout Account in the system**
>
> > id should be the unique identifier for an existing Payout Account
> > **_rest of the request fields are self explanatory_**

```python
import json

request = {
  "id": "PZZA",
  "name": "Johns Pizza Inc.",
  "website": "www.pizzapizza.com",
  "address1": "123 Main Street",
  "address2": "Suite 2001",
  "city": "Chicago",
  "state": "IL",
  "zip": "60004",
  "country": "US",
  "contactName": "John Smith",
  "contactPhone": "312-899-8999",
  "contactEmail": "john@pizzapizza.com",
  "status": "A"
}

print("UPDATE PAYOUT ACCOUNT request >>>")
print(json.dumps(request, indent=2))

response = payout.UpdatePayoutAccount(request)

print("UPDATE PAYOUT ACCOUNT response >>>")
print(json.dumps(response, indent=2))

```

**_UpdatePayoutAccount Request Example_**

```python
{
  "id": "PZZA",
  "name": "Johns Pizza Inc.",
  "website": "www.pizzapizza.com",
  "address1": "123 Main Street",
  "address2": "Suite 2001",
  "city": "Chicago",
  "state": "IL",
  "zip": "60004",
  "country": "US",
  "contactName": "John Smith",
  "contactPhone": "312-899-8999",
  "contactEmail": "john@pizzapizza.com",
  "status": "A"
}
```

**_UpdatePayoutAccount Response_**

```python
{
  "id": "PZZA",
  "app": "DEMO",
  "logoUrl": null,
  "name": "Johns Pizza Inc.",
  "address1": "123 Main Street",
  "address2": "Suite 2001",
  "city": "Chicago",
  "state": "IL",
  "zip": "60004",
  "country": "US",
  "contactName": "John Smith",
  "contactPhone": "312-899-8999",
  "contactEmail": "john@pizzapizza.com",
  "website": "www.pizzapizza.com",
  "status": "A"
}
```

---

### Find a Payout Account by Id

> **Get an existing Payout Account in the system**
>
> > id should be the unique identifier for an existing Payout Account
> > **_rest of the request fields are self explanatory_**

```python
import json

request = {
  "id": "PZZA",
}

print("FIND PAYOUT ACCOUNT request >>>")
print(json.dumps(request, indent=2))

response = payout.FindPayoutAccount(request)

print("FIND PAYOUT ACCOUNT response >>>")
print(json.dumps(response, indent=2))

```

**_FindPayoutAccount Request Example_**

```python
{
  "id": "PZZA",
}
```

**_FindPayoutAccount Response_**

```python
{
  "id": "PZZA",
  "app": "DEMO",
  "logoUrl": null,
  "name": "Johns Pizza Inc.",
  "address1": "123 Main Street",
  "address2": "Suite 2001",
  "city": "Chicago",
  "state": "IL",
  "zip": "60004",
  "country": "US",
  "contactName": "John Smith",
  "contactPhone": "312-899-8999",
  "contactEmail": "john@pizzapizza.com",
  "website": "www.pizzapizza.com",
  "status": "A"
}
```

---

## _Payout Token API Functions_

---

> **_Using RedPay API Create a new Token for an ACH Account in the system which will later be configure to receive payouts from the Red Shepherd payout scheduler_**

> To Create an ACH token use the Redpay API Tokenize ACH Account method follow this documentation

> Library is hosted on pypi https://pypi.org/project/redpay/

> Once the Token is created for the account for the Receiving party, you can then add it to the Payout Account using the following Payout API Calls

---

### Add a Payout Token to an Existing Payout Account

> **_Add a ACH Payment token which was tokenized using the redpay api to attach it to an existing Payout Account_**
>
> > id should be unique identifier for each Payout Account
>
> > token is the ACH Account token for PZZA Account where they would like to get their deposits for their payouts
> >
> > > note that if the payout account ACH information needs to be updated, just follow the same steps and add a new token to the same account

```python
import json

request = {
  "id": "PZZA",
  "token": "fojid27g24u57zp1"
}

print("ADD PAYOUT TOKEN request >>>")
print(json.dumps(request, indent=2))

response = payout.AddPayoutToken(request)

print("ADD PAYOUT TOKEN response >>>")
print(json.dumps(response, indent=2))

```

**AddPayoutToken Request Example**

```python
{
  "id": "PZZA",
  "token": "fojid27g24u57zp1"
}
```

**AddPayoutToken Response**

```python
{
  "id": "DEMO.PZZA.3",
  "app": "DEMO",
  "account": "PZZA",
  "version": 3,
  "token": "fojid27g24u57zp1",
  "timestamp": "2021-03-02T22:49:52.4505780Z",
  "userId": null
}
```

---

---

## _Payout Rate API Functions_

---

---

> **_Add a Payout Rate to an existing Payout Account_**
>
> > Payout Rates control the amounts/ frequencies and other parameters for controlling the payout for each Payout Account

### Add a Payout Rate to an Existing Payout Account

> > id - unique identifier for each Payout Account
>
> > trxPct - how much percent of each transaction should be given to the payout account
>
> > trxFee - flat fee in cents for each transaction to be given to the payout account
>
> > monthlyFee - monthly fee to be paid to the payout account, will be paid on monthlyPayDate day of the month
>
> > monthlyPayDate - date on which the monthly fee is to be paid, (defaults to 1 if not set)
>
> > yearlyFee - yearly fee to be paid to the payout account, will be paid on yearlyPayMonth month of the year
>
> > yearlyPayMonth - month which the monthly fee is to be paid, (defaults to 1 (January) if not set)
>
> > payFrequency - payment frequency for transaction based payments,
> >
> > > D - Daily
> > > W - Weekly
> > > M - Monthly
>
> > startDate - payout start date (optional)
>
> > endDate - payout end date (optional)
>
> > autoApproveLimit - anything under this limit is automatically paid, if you set this then any payout over this limit will go to review first

```python
import json

request = {
  "id": "PZZA",
  # give payout account 80% of each transaction
  "trxPct": 80,
  # give payout account 1 dollar (100 cents) flat fee per transaction
  "trxFee": 100
}

print("ADD PAYOUT RATE request >>>")
print(json.dumps(request, indent=2))

response = payout.AddPayoutRate(request)

print("ADD PAYOUT RATE response >>>")
print(json.dumps(response, indent=2))

```

**AddPayoutRate Request Example**

```python
{
  "id": "PZZA",
  "trxPct": 80,
  "trxFee": 100
}
```

**AddPayoutRate Response**

```python
{
  "id": "DEMO.PZZA.1",
  "app": "DEMO",
  "account": "PZZA",
  "version": 1,
  "trxPct": 80,
  "trxFee": 100,
  "monthlyFee": 0,
  "monthlyPayDate": 0,
  "yearlyFee": 0,
  "yearlyPayMonth": 0,
  "status": null,
  "payFrequency": null,
  "startDate": null,
  "endDate": null,
  "autoApproveLimit": 0,
  "lastMontlyPayout": null,
  "lastYearlyPayout": null,
  "lastDailyTrxPayout": null,
  "lastWeeklyTrxPayout": null,
  "lastMontlyTrxPayout": null,
  "timestamp": "2021-02-28T19:00:19.1518640Z",
  "userId": null
}
```

---

### Start Payouts

> **Start Payouts for an Existing Account**
>
> > id should be the unique identifier for an existing Payout Account
>
> > "status": "A" means payout has been activated, so make sure you see that in the response

```python
import json

request = {
  "id": "PZZA",
}

print("START PAYOUTS request >>>")
print(json.dumps(request, indent=2))

response = payout.StartPayouts(request)

print("START PAYOUTS response >>>")
print(json.dumps(response, indent=2))

```

**StartPayouts Request Example\_**

```python
{
  "id": "PZZA",
}
```

**StartPayouts Response\_**

```python
{
  "id": "DEMO.PZZA.6",
  "app": "DEMO",
  "account": "PZZA",
  "version": 6,
  "trxPct": 80,
  "trxFee": 100,
  "monthlyFee": 0,
  "monthlyPayDate": 0,
  "yearlyFee": 0,
  "yearlyPayMonth": 0,
  "status": "A",
  "payFrequency": null,
  "startDate": null,
  "endDate": null,
  "autoApproveLimit": 0,
  "lastMontlyPayout": null,
  "lastYearlyPayout": null,
  "lastDailyTrxPayout": null,
  "lastWeeklyTrxPayout": null,
  "lastMontlyTrxPayout": null,
  "timestamp": "2021-03-01T23:34:36.1005288Z",
  "userId": null
}
```

---

### Stop Payouts

> **Stop Payouts for an Existing Account**
>
> > id should be the unique identifier for an existing Payout Account
>
> > "status": "D" means payout has been deactivated, so make sure you see that in the response

```python
import json

request = {
  "id": "PZZA",
}

print("STOP PAYOUTS request >>>")
print(json.dumps(request, indent=2))

response = payout.StopPayouts(request)

print("STOP PAYOUTS response >>>")
print(json.dumps(response, indent=2))

```

**StopPayouts Request Example**

```python
{
  "id": "PZZA",
}
```

**StopPayouts Response**

```python
{
  "id": "DEMO.PZZA.7",
  "app": "DEMO",
  "account": "PZZA",
  "version": 7,
  "trxPct": 80,
  "trxFee": 100,
  "monthlyFee": 0,
  "monthlyPayDate": 0,
  "yearlyFee": 0,
  "yearlyPayMonth": 0,
  "status": "D",
  "payFrequency": null,
  "startDate": null,
  "endDate": null,
  "autoApproveLimit": 0,
  "lastMontlyPayout": null,
  "lastYearlyPayout": null,
  "lastDailyTrxPayout": null,
  "lastWeeklyTrxPayout": null,
  "lastMontlyTrxPayout": null,
  "timestamp": "2021-03-01T23:34:36.1005288Z",
  "userId": null
}
```
