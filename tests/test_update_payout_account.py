import json

from payout import Payout

if __name__ == "__main__":
    payout = Payout("DEMO",
                    "MIIBojANBgkqhkiG9w0BAQEFAAOCAY8AMIIBigKCAYEAtsQxNp3vmKVNYIxfWSi0LIRgCnPaMn0MUNouxgrs4zmg4cnvSeQ3I8YP03YbpXuWA80RvOw/nWErYAKomniJw8Y+xexMfBQ5sgJgewn3ZnRPNM9Y4Z62gwfIlsrs7Bwvpz9uUtLgeQLl1ffNaumnu1IBrqRps0EZ1QyDuu41UckTyo31C40Wez6IbeMfZeusrmPlIWqyBacdviJ5zHCA3zHNq86QMnB8HOP1U81HOSs6GTTelhD7lCoJ+fHKHxcz0MDr37fNpKpC57B0/20wBXFp9tlVtSkHcIty1lyNk2/HDH8knCdqkZk+fCvWgGwdex41x8/rM+LKC13c5J/yG6Gb2PnKhwNk4lvvnz73YAdqTUJ7qNrdtWVnOTWfbMBiNlpBCVqt8xY8UK6u83AVWrWXse0xe2Pn/kRqlXmxWT0mGEoCavjvZ9lQUL7LXAXZ1dff9r+oFUZo6xDQ3ER/OTIKa4jpvaI9S/J1drsrI1f9kkMWFwEh48dCPYplGSxzAgMBAAE=")

    request = {
        "id": "PZZA",
        "name": "Johns Pizza Inc.",
        "website": "www.pizzapizza.com",
        "address1": "123 Main Street",
        "address2": "Suite 2001",
        "city": "Chicago",
        "state": "IL",
        "zip": "60004",
        "country": "US",
        "contactName": "John Smith",
        "contactPhone": "312-899-8999",
        "contactEmail": "john@pizzapizza.com",
        "status": "A"
    }

    print("UPDATE PAYOUT ACCOUNT request >>>")
    print(json.dumps(request, indent=2))

    response = payout.UpdatePayoutAccount(request)

    print("UPDATE PAYOUT ACCOUNT response >>>")
    print(json.dumps(response, indent=2))
